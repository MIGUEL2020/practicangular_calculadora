import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  @Output() pasarFirst= new EventEmitter();
  @Output() pasarSecond = new EventEmitter();
  @Output() pasarOperacion = new EventEmitter();


  firstNum;
  secondNum;

  ngOnInit() {
  }

  onPasarAPadre(operacion : string){
    this.pasarFirst.emit(this.firstNum);
    this.pasarSecond.emit(this.secondNum);
    this.pasarOperacion.emit(operacion);
  }


}
