import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'calculadoraVersion2';

  firstNumberPadre;
  secondNumberPadre;
  resultado;

  recibirFirstFromHijo(n : number){
    this.firstNumberPadre = n;
  }
  recibirSecondFromHijo(n : number){
    this.secondNumberPadre = n;
  }
  recibirOperacionFromHijo(o : string){
      if(o=="+"){
        this.resultado = this.firstNumberPadre + this.secondNumberPadre;
      }else if(o=="-"){
        this.resultado = this.firstNumberPadre - this.secondNumberPadre;
      }else if(o=="*"){
        this.resultado = this.firstNumberPadre * this.secondNumberPadre;
      }else if(o=="/"){
        this.resultado = this.firstNumberPadre / this.secondNumberPadre;
      } 
  }

}
